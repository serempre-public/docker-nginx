# Docker Nginx 1.22 on Alpine Linux
Example Nginx 1.22 container image for Docker, built on [Alpine Linux](https://www.alpinelinux.org/).

Original Repository: https://github.com/TrafeX/docker-php-nginx

* Built on the lightweight and secure Alpine Linux distribution
* Multi-platform, supporting AMD4, ARMv6, ARMv7, ARM64
* Very small Docker image size (+/-40MB)
* Optimized for 100 concurrent users
* The services Nginx and supervisord run under a non-privileged user (nobody) to make it more secure
* The logs of all the services are redirected to the output of the Docker container (visible with `docker logs -f <container name>`)
* Follows the KISS principle (Keep It Simple, Stupid) to make it easy to understand and adjust the image to your needs

![nginx 1.22](https://img.shields.io/badge/nginx-1.22-brightgreen.svg)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)

## Goal of this project
The goal of this container image is to provide an example for running Nginx in a container which follows
the best practices and is easy to understand and modify to your needs.

## Usage

Start the Docker container:

    docker run -p 80:8080 registry.gitlab.com/serempre-public/docker-nginx:latest

See the PHP info on http://localhost, or the static html page on http://localhost/test.html

Or mount your own code to be served by PHP-FPM & Nginx

    docker run -p 80:8080 -v ~/my-codebase:/var/www/html registry.gitlab.com/serempre-public/docker-nginx:latest


## Configuration
In [config/](config/) you'll find the default configuration files for Nginx, PHP and PHP-FPM.
If you want to extend or customize that you can do so by mounting a configuration file in the correct folder;

Nginx configuration:

    docker run -v "`pwd`/nginx-server.conf:/etc/nginx/conf.d/server.conf" registry.gitlab.com/serempre/docker-nginx:latest


_Note; Because `-v` requires an absolute path I've added `pwd` in the example to return the absolute path to the current directory_
